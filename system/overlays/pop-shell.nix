final: prev:
{
  gnomeExtensions = prev.gnomeExtensions // {
    pop-shell = prev.gnomeExtensions.pop-shell.overrideAttrs (old: {
      src = prev.fetchFromGitHub {
        owner = "pop-os";
        repo = "shell";
        rev = "104269ede04d52caf98734b199d960a3b25b88df";
        hash = "sha256-rBu/Nn7e03Pvw0oZDL6t+Ms0nesCyOm4GiFY6aYM+HI=";
      };
    });
  };
}
