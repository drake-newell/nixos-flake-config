# Specific configuration for my Framework laptop (12th Gen Intel)

{ config, pkgs, ... }:

{
  # Allow unfree packages.
  nixpkgs.config.allowUnfree = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Configuration options for LUKS Device
  boot.initrd.luks.devices = {
    crypted = {
      device = "/dev/disk/by-partuuid/5ce31db1-37a1-405c-a64f-2fdba654a1f0";
      header = "/dev/disk/by-partuuid/09e827b1-a43a-47d0-addf-009e7b7887c7";
      allowDiscards = true; # Used if primary device is a SSD
      preLVM = true;
    };
  };

  # enable deep sleep
  boot.kernelParams = [ "mem_sleep_default=deep" ];

  # enable fingerprint reader
  services.fprintd.enable = true;

  virtualisation.docker.enable = true;
  users.users.drake.extraGroups = [ "docker" ];


}
