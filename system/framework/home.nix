# Specific configuration for my Framework 13

{ config, pkgs, ... }:

{
  imports = [
    ../home-shared.nix
  ];

  home.packages = with pkgs; [
    neovim
    spotify
    discord
    nordic
    lf
    sxiv
    alacritty
    kitty
    emacs
    #zathura (not building)
    mupdf # replacing zathura
    signal-desktop
    transmission_4-gtk
  ];
}
