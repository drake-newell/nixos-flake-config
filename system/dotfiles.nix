# My dotfiles configuration

{ config, pkgs, lib, ... }:

{

  home.file = {
    ".config/alacritty/alacritty.toml".source = ../dotfiles/config/alacritty/alacritty.toml;
    ".config/kitty/kitty.conf".source = ../dotfiles/config/kitty/kitty.conf;

  };
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    # Configuration written to ~/.config/starship.toml
    settings = {
      add_newline = false;
      character = {
      success_symbol = "[➜](bold green)";
      error_symbol = "[➜](bold red)";
      };
    };
  };
  programs.zsh = {
    enable = true;
    syntaxHighlighting.enable = true;
    shellAliases = {
      ls="ls -hN --color=auto --group-directories-first";
      grep="grep --color=auto";
      diff="diff --color=auto";
      ccat="highlight --out-format=ansi";
      update = "sudo nixos-rebuild switch --flake $HOME/.nixos-flake-config#";
      nx = "cd $HOME/.nixos-flake-config";
      cp="cp -iv";
      mv="mv -iv";
      rm="rm -v";
      absig="java -jar boot_signer.jar /boot boot.img custom.pk8 custom.x509.der boot_signed.img";
      arsig="java -jar boot_signer.jar /recovery recovery.img custom.pk8 custom.x509.der recovery_signed.img";
      ka="killall";
      g="git";
      gc="git commit" ;
      gp="git push" ;
      gpl="git pull"; 
      gplu="git pull upstream master";
      gcl="git clone";
      trem="transmission-remote";
      lrbbf="sudo flashrom -c MX25L6405 -p internal:boardmismatch=force,laptop=force_I_want_a_brick -w";
      v="nvim";
      sn="sudo nvim";
      xp="cd ~/Projects/void-packages && git pull --no-edit upstream master && ./xbps-src bootstrap-update && git push";
      xgt="xgensum -f -i template";
      vimg="cd ~/.local/src/void-mklive && sudo ./mklive.sh -r http://198.255.68.182 -T 'Void GNU/Linux-libre'";
    };
    history = {
      size = 10000;
      path = ".local/share/zsh/history";
    };
    zplug = {
      enable = true;
      plugins = [
        { name = "zsh-users/zsh-autosuggestions"; } # Simple plugin installation
        { name = "jeffreytse/zsh-vi-mode"; } # Simple plugin installation
      ];
    };
  };
}
