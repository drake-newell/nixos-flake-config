{
  description = "Drake Newell's NixOS configuration flake";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager/master";
  };

  outputs = { nixpkgs, home-manager, ... }: {
    nixosConfigurations = {
      hephaestus = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
	modules = [
	  ./system/configuration.nix
	  ./system/framework/device.nix
	  ./system/framework/hardware-configuration.nix
	  home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
	    home-manager.backupFileExtension = "backup";
            home-manager.users.drake = import ./system/framework/home.nix;
	  }
	];
      };
    };
  };
}
